
import HomePage from '../pages/home.jsx';
import LoginPage from '../pages/login.jsx'

var routes = [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/login/',
    component: LoginPage
  }
];

export default routes;
