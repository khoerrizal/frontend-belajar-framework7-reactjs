import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  BlockTitle,
  List,
  ListItem,
  Link,
  Searchbar,
  Icon,
  Toolbar,
  Block,
} from 'framework7-react';
import Framework7 from 'framework7/types';
import React, { useState, useEffect } from 'react';


export default ({ f7router }) => {
  useEffect(() => {
    const status = localStorage.getItem("status")
    if (!status || status != 'login'){
      f7router.navigate('/login/')
    }
  }, []);
  return (
    <Page name="home">
      {/* Top Navbar */}
      <Navbar large>
        <NavTitle>AppLatihan</NavTitle>
        <NavTitleLarge>AppLatihan</NavTitleLarge>
      </Navbar>
      {/* Toolbar */}
      <Toolbar bottom>
        <Link>Left Link</Link>
        <Link>Right Link</Link>
      </Toolbar>
      {/* Page content */}
      <Block strong>
        <p>Here is your blank Framework7 app. Let's see what we have here.</p>
      </Block>
      <Link href="/login/">LOGIN</Link>

    </Page>
  )
};