import React, { useState } from 'react';
import {
  f7,
  Page,
  LoginScreenTitle,
  List,
  ListInput,
  ListButton,
  BlockFooter,
} from 'framework7-react';
import Framework7 from 'framework7/types';

export default ({ f7router }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const signIn = () => {
    Framework7.request.post('http://localhost/backend-belajar-framework7/login.php', { username:username, password: password })
        .then(function (res) {
            if (res.data){
                const jsondata = JSON.parse(res.data)
                console.log(jsondata, "<<<< cek")
                localStorage.setItem("status", "login");
                localStorage.setItem("user_id", res.data.id)
                f7router.navigate('/')
            }
    });
  };

  return (
    <Page noToolbar noNavbar noSwipeback loginScreen>
  <LoginScreenTitle>Framework7</LoginScreenTitle>
  <List form>
    <ListInput
      label="Username"
      type="text"
      placeholder="Your username"
      value={username}
      onInput={(e) => {
        setUsername(e.target.value);
      }}
    />
    <ListInput
      label="Password"
      type="password"
      placeholder="Your password"
      value={password}
      onInput={(e) => {
        setPassword(e.target.value);
      }}
    />
  </List>
  <List>
    <ListButton onClick={signIn}>Sign In</ListButton>
    <BlockFooter>
      Some text about login information.
      <br />
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    </BlockFooter>
  </List>
    </Page>
  );
};